package Activitat13;

public class TestFecha {
    public static void main(String[] args) {
        System.out.println("=== START - Prueba de Constructores - START ===");
        System.out.println("--- Creo un nuevo objeto utilizando el constructor parametrizado int\n" +
                "int int (16,1,2021) ---");
        Fecha fecha1 = new Fecha(16,1,2021);
        fecha1.mostrarFormatoES();
        fecha1.mostrarFormatoGB();
        fecha1.mostrarFormatoTexto();
        System.out.println("\nla fecha es: "+fecha1.isCorrecta());
        System.out.println("El dia de la semana es: "+fecha1.getDiaSemana());
        System.out.println("la fecha es festivo: "+fecha1.isFestivo());
        System.out.println("-------------------------------\n");
        System.out.println("--- Creo un nuevo objeto utilizando la fecha anterior mediante el\n" +
                "constructor por copia ---");
        Fecha fechacopia = fecha1.clone();
        fechacopia.mostrarFormatoES();
        fechacopia.mostrarFormatoGB();
        fechacopia.mostrarFormatoTexto();
        System.out.println("\nla fecha es: "+fechacopia.isCorrecta());
        System.out.println("la fecha es festivo: "+fechacopia.isFestivo());
        System.out.println("El dia de la semana es: "+fechacopia.getDiaSemana());
        System.out.println("-------------------------------\n");

        System.out.println("--- Compruebo mediante el método equals que las fechas representadas\n" +
                "por ambos objetos son iguales ---");
        System.out.println("La fecha creada con los constructores anteriores son iguales: "+fecha1.isEqual(fechacopia));
        System.out.println("-------------------------------\n");

        System.out.println("--- Creo un nuevo objeto utilizando el constructor String 16/1/2021 ---");
        Fecha fechatexto = new Fecha("16/1/2021");
        fechatexto.mostrarFormatoES();
        fechatexto.mostrarFormatoGB();
        fechatexto.mostrarFormatoTexto();
        System.out.println("\nla fecha es: "+fechatexto.isCorrecta());
        System.out.println("la fecha es festivo: "+fechatexto.isFestivo());
        System.out.println("El dia de la semana es: "+fechatexto.getDiaSemana());
        System.out.println("-------------------------------\n");

        System.out.println("--- Creo un nuevo objeto utilizando el constructor por defecto ---");
        Fecha fechaPorDefecto = new Fecha();
        fechaPorDefecto.mostrarFormatoES();
        fechaPorDefecto.mostrarFormatoGB();
        fechaPorDefecto.mostrarFormatoTexto();
        System.out.println("\nla fecha es: "+fechaPorDefecto.isCorrecta());
        System.out.println("la fecha es festivo: "+fechaPorDefecto.isFestivo());
        System.out.println("El dia de la semana es: "+fechaPorDefecto.getDiaSemana());
        System.out.println("-------------------------------\n");

        System.out.println("=== FIN - Prueba de Constructores - FIN ===\n");

        System.out.println("=== START - Prueba de Métodos anyadir/restar días - START ====\n");

        System.out.println("--- Día siguiente a la fecha inicial (16-1-2021) - (+1 día) ---");
        fecha1.anyadir(1);
        fecha1.mostrarFormatoES();
        fecha1.mostrarFormatoGB();
        fecha1.mostrarFormatoTexto();
        System.out.println("\nla fecha es: "+fecha1.isCorrecta());
        System.out.println("la fecha es festivo: "+fecha1.isFestivo());
        System.out.println("El dia de la semana es: "+fecha1.getDiaSemana());
        System.out.println("-------------------------------\n");

        System.out.println("--- Día anterior a la fecha inicial (16-1-2021) - (-1 día) ---");
        fecha1.restar(1);
        fecha1.mostrarFormatoES();
        fecha1.mostrarFormatoGB();
        fecha1.mostrarFormatoTexto();
        System.out.println("\nla fecha es: "+fecha1.isCorrecta());
        System.out.println("la fecha es festivo: "+fecha1.isFestivo());
        System.out.println("El dia de la semana es: "+fecha1.getDiaSemana());
        System.out.println("-------------------------------\n");

        System.out.println("--- Fecha correspondiente a restar 30 días a la fecha inicial\n" +
                "(16-1-2021) - (-30 días) ---");
        fecha1.restar(30);
        fecha1.mostrarFormatoES();
        fecha1.mostrarFormatoGB();
        fecha1.mostrarFormatoTexto();
        System.out.println("\nla fecha es: "+fecha1.isCorrecta());
        System.out.println("la fecha es festivo: "+fecha1.isFestivo());
        System.out.println("El dia de la semana es: "+fecha1.getDiaSemana());
        System.out.println("-------------------------------\n");

        System.out.println("=== FIN - Prueba de Métodos anyadir/restar - FIN ===\n");

        System.out.println("=== START - Prueba del método modificador - START ===\n");

        System.out.println("--- Modifico la fecha del primer objeto creado (16-1-2020) por la fecha\n" +
                "22-1-2021 ---");
        fecha1.set(22,1,2021);
        fecha1.mostrarFormatoES();
        fecha1.mostrarFormatoGB();
        fecha1.mostrarFormatoTexto();
        System.out.println("\nla fecha es: "+fecha1.isCorrecta());
        System.out.println("la fecha es festivo: "+fecha1.isFestivo());
        System.out.println("El dia de la semana es: "+fecha1.getDiaSemana());
        System.out.println("-------------------------------\n");




    }
}
