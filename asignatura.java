//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package Activitat9;

public class asignatura {
    private String nombre;
    private String codigo;
    private String curso;
    private boolean optativo;

    public asignatura(String nombre, String codigo, String curso, boolean optativo) {
        this.nombre = nombre;
        this.codigo = codigo;
        this.curso = curso;
        this.optativo = optativo;
    }

    public void mostrarNombre() {
        System.out.println(this.nombre);
    }

    public void mostrarCodigo() {
        System.out.println(this.codigo);
    }

    public void mostrarCurso() {
        System.out.println(this.curso);
    }

    public void mostrarOptativa() {
        if (this.optativo) {
            System.out.println("Es optativa");
        } else {
            System.out.println("No es optativa");
        }

    }
}
