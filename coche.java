package Activitat8;

public class coche {
    private String marca;
    private String modelo;
    private String color;
    private boolean metalizado;
    private String matricula;
    private int anyo;

    public coche(String marca, String modelo, String color, boolean metalizado, String matricula, int anyo) {
        this.marca = marca;
        this.modelo = modelo;
        this.color = color;
        this.metalizado = metalizado;
        this.matricula = matricula;
        this.anyo = anyo;
    }

    public coche() {
        this.marca = "Mclaren";
        this.modelo = "720s";
        this.color = "Naranja Papaya";
        this.metalizado = true;
        this.matricula = "3456KMZ";
        this.anyo = 2019;
    }

    public void mostrarInfo() {
        System.out.println(this.modelo + " " + this.color);
    }
}

