package Activitat13;

import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;

class Fecha {

    private int dia;

    private int mes;

    private int anyo;

    private static final String[] DIAS_TEXTO = new String[] { "domingo", "lunes", "martes", "miercoles", "jueves", "viernes",
            "sábado"};

    private static final String[] MESES_TEXTO = new String[] { "enero", "febrero", "marzo", "abril", "mayo", "junio",
            "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre" };

    /**
     *  Constructor por defecto
     *  Inicializa una fecha a dia 1-1-1970
     */
    public Fecha() {
        // tu codigo aqui
        this.dia = 01;
        this.mes = 01;
        this.anyo = 1970;
    }

    /**
     *  Inicializa la fecha
     *  @param dia de la semana
     *  @param mes del año
     *  @param anyo
     */
    public Fecha(int dia, int mes, int anyo) {
        // Tu código aquí
        this.dia = dia;
        this.mes = mes;
        this.anyo = anyo;
    }

    /**
     * Inicializa la fecha a partir de otra pasada en formato String dd/mm/yyyy
     *
     * Deberemos trocearlas de forma que asignemos el día més y año a cada uno de los atributoe
     * @param fecha
     */
    public Fecha(String fecha) {
        // Tu código aquí
        StringBuilder dia = new StringBuilder();
        StringBuilder mes = new StringBuilder();
        StringBuilder anyo = new StringBuilder();
        int contador = 0;
        for (int i = 0; i < fecha.length(); i++) {

            if (String.valueOf(fecha.charAt(i)).equals("/")){
                contador++;
            }
            if (contador == 0){
                dia.append(fecha.charAt(i));
            }else if (contador == 1 && !String.valueOf(fecha.charAt(i)).equals("/")){
                mes.append(fecha.charAt(i));
            }else if (contador == 2 && !String.valueOf(fecha.charAt(i)).equals("/")){
                anyo.append(fecha.charAt(i));
            }
        }
        this.dia = Integer.parseInt(dia.toString());
        this.mes = Integer.parseInt(mes.toString());
        this.anyo = Integer.parseInt(anyo.toString());
    }

    /**
     * Modifica la fecha actual a partir de los datos pasados como argumento
     */
    public void set(int dia, int mes, int anyo) {
        // Tu código aquí
        this.dia = dia;
        this.mes = mes;
        this.anyo = anyo;
    }

    /**
     * Obtiene una fecha con los mismos atributos que la fecha actual
     * @return
     */
    public Fecha clone() {
        return new Fecha(this.dia,this.mes,this.anyo);
    }

    /**
     * Devuelve el día de la semana que representa por la Fecha actual
     * @return @dia
     */
    public int getDia() {
        return this.dia;
    }

    /**
     * Devuelve el mes que representa la Fecha actual
     * @return @mes
     */
    public int getMes(){
        return this.mes;
    }

    /**
     * Devuelve el año que representa la Fecha actual
     * @return @mes
     */
    public int getAnyo(){
        return this.anyo;
    }

    /**
     * Muestra por pantalla la fecha en formato español dd-mm-yyyy
     */
    public void mostrarFormatoES()  {
        // Tu código aquí
        System.out.printf("%02d-%02d-%04d\n",this.dia,this.mes,this.anyo);
    }

    /**
     * Muestra por pantalla la fecha en formato inglés yyyy-mm-dd
     */
    public void mostrarFormatoGB() {
        // Tu código aquí
        System.out.printf("%04d-%02d-%02d\n",this.anyo,this.mes,this.dia);
    }

    /**
     * Muestra por pantalla la fecha en formato texto dd-mmmm-yyyy
     * Ej. 1 enero de 1970
     */
    public void mostrarFormatoTexto() {
        // Tu código aquí
        System.out.printf("%02d-"+MESES_TEXTO[this.mes-1]+"-%04d",this.dia,this.anyo);
    }

    /**
     * Retorna un booleano indicando si la fecha del objeto es igual a la fecha pasada como
     * argumento
     *
     * @return boolean
     */
    public boolean isEqual(Fecha otraFecha) {
        if (this.dia == otraFecha.dia && this.mes == otraFecha.mes && this.anyo == otraFecha.anyo){
            return true;
        }else {
            return false;
        }
    }

    /**
     * Devuelve el número de la semana dentro del año actual.
     *
     * @return int dia semana
     */
    public int getNumeroSemana() {
        int dias = 0;
        for (int i = 1; i < this.anyo; i++) {
            dias = dias + getDiasAnyo(i);
        }
        int primerDia = (dias+1)%7;
        int primeraSemana;
        if (primerDia == 0){
            primeraSemana = 1;
        }else if (primerDia == 1){
            primeraSemana = 7;
        }else if (primerDia == 2){
            primeraSemana = 6;
        }else if (primerDia == 3){
            primeraSemana = 5;
        }else if (primerDia == 4){
            primeraSemana = 4;
        }else if (primerDia == 5){
            primeraSemana = 3;
        }else {
            primeraSemana = 2;
        }
        int semanaActual = (getDiasTranscurridosAnyo()-primeraSemana)/7+2;
        return semanaActual;
    }

    /**
     * Retorna el dia correspondiente de la semana en formato String
     * @return String
     */
    public String getDiaSemana() {
        int dia = getDiasTranscurridosOrigen()%7;
        return DIAS_TEXTO[dia];
    }

    /**
     * Solo Festivo sábado o domingo
     * @return boolean
     */
    public boolean isFestivo() {
        int festivo = getDiasTranscurridosOrigen()%7;
        if (festivo == 0 || festivo == 6){
            return true;
        }else {
            return false;
        }
    }

    /**
     * Devuelve un objeto de tipo fecha que representa una fecha añadiendo @numDias
     * A la fecha Actual. El número máximo de dias a restar es 30
     *
     * @return boolean
     */
    public Fecha anyadir(int numDias){
        if (numDias <=30 && numDias > 0){
            for (int i = 1; i <= numDias; i++) {
                this.dia = this.dia + 1;
                if ((this.mes == 2 && this.dia == 28) && !isBisiesto(this.anyo)){
                    this.dia = 1;
                    this.mes = 3;
                }else if ((this.mes == 2 && this.dia == 29) && isBisiesto(this.anyo)){
                    this.dia = 1;
                    this.mes = 3;
                }else if (this.dia == 31 &&(this.mes == 1 || this.mes == 3 || this.mes == 5 || this.mes == 7 || this.mes == 8 || this.mes == 10)){
                    this.dia = 1;
                    this.mes = this.mes +1;
                }else if (this.dia == 31 && this.mes == 12){
                    this.dia = 1;
                    this.mes = 1;
                    this.anyo++;
                }else if (this.dia == 30 && (this.mes == 4 || this.mes == 6 || this.mes == 9 || this.mes == 11)){
                    this.dia = 1;
                    this.mes++;
                }
            }


        }else {
            System.out.println("Error");
        }

        return new Fecha(this.dia,this.mes,this.anyo);
    }

    /**
     * Devuelve un objeto de tipo fecha que representa una fecha restando @numDias
     * A la fecha Actual. El número máximo de dias a restar es 30
     *
     * @return boolean
     */
    public Fecha restar(int numDias){
        if (numDias <=30 && numDias > 0){
            for (int i = numDias; i >= 1; i--) {
                this.dia--;
                if ((this.mes == 3 && this.dia == 0) && !isBisiesto(this.anyo)){
                    this.dia = 28;
                    this.mes = 2;
                }else if ((this.mes == 3 && this.dia == 0) && isBisiesto(this.anyo)){
                    this.dia = 29;
                    this.mes = 2;
                }else if (this.dia == 0 && this.mes == 1){
                    this.dia = 31;
                    this.mes = 12;
                    this.anyo--;
                }else if (this.dia == 0 &&(this.mes == 2 || this.mes == 5 || this.mes == 7 || this.mes == 8 || this.mes == 10)){
                    this.dia = 30;
                    this.mes--;
                }else if (this.dia == 0 && (this.mes == 4 || this.mes == 6 || this.mes == 9 || this.mes == 11)){
                    this.dia = 31;
                    this.mes--;
                }
            }


        }else {
            System.out.println("Error");
        }

        return new Fecha(this.dia,this.mes,this.anyo);
    }

    public boolean isCorrecta(){
        if (this.dia > getDiasMes(this.mes, this.anyo) || this.mes > 12){
            return false;
        }else {
            return true;
        }
    }

    /**
     * Retorna el mes del año en formato text (enero, febrero, marzo,...)
     * @return char
     */
    private String getMesTexto() {
        return MESES_TEXTO[this.mes-1];
    }

    /**
     * Devuelve el número de dias transcurridos desde el 1-1-1
     *
     * @return int
     */
    private int getDiasTranscurridosOrigen() {
        int dias = 0;
        for (int i = 1; i < this.anyo; i++) {
            dias = dias + getDiasAnyo(i);
        }
        dias = dias + getDiasTranscurridosAnyo();
        return dias;
    }

    /**
     * Devuelve el número de dias transcurridos en el anyo que representa el objeto
     *
     * @return int
     */
    private int getDiasTranscurridosAnyo() {
        int dias = 0;
        for (int i = 1; i < this.mes; i++) {
            dias = dias + getDiasMes(i,this.anyo);
        }
        dias = dias + this.dia;
        return dias;
    }

    /**
     * Indica si el año pasado como argumento es bisiesto. Un año es bisiesto si es divisible por 4
     * pero no es divisible entre 100 o es divisible entre 4 entre 100 y entre 400
     *
     * @return boolean
     */
    public static boolean isBisiesto(int anyo){
        if ((anyo % 4 == 0) && ((anyo % 100 != 0) || (anyo % 400 == 0))){
            return true;
        }
        else{
            return false;
        }
    }
    /**
     *  Calcula el número de días que tiene el @mes en el @año pasado como argumento
     *  Deberás hacer uso del métodos isBisiesto
     *
     *  @return int total dias mes en curso
     */
    public static int getDiasMes(int mes, int anyo) {
        if (isBisiesto(anyo) && mes == 2){
            return 29;
        }else if (!isBisiesto(anyo) && mes == 2){
            return 28;
        }else if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12){
            return 31;
        }else {
            return 30;
        }
    }
    /**
     * Calcula el número total de dias que tiene el año pasado como argumento
     *
     * @return int total dias anyo en curso
     */
    public static int getDiasAnyo(int anyo){
        if (isBisiesto(anyo)){
            return 366;
        }else {
            return 365;
        }
    }
}

