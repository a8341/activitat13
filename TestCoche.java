//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package Activitat8;

public class TestCoche {
    public TestCoche() {
    }

    public static void main(String[] args) {
        coche seat = new coche("Seat", "Ibiza", "Rojo", true, "2212KVN", 2000);
        coche ferrari = new coche("Ferrari", "Moderna", "Negro", true, "2122RRN", 1985);
        coche volkswagen = new coche("volkswagen", "Tiguan", "Blanco", false, "1212KJ", 2015);
        coche opel = new coche("Opel", "Corsa", "Verde", true, "56355R", 2003);
        seat.mostrarInfo();
        ferrari.mostrarInfo();
        volkswagen.mostrarInfo();
        opel.mostrarInfo();
    }
}
